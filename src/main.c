#include <stdio.h>

int main() {
	printf("[?25l[?7l[0m[36m[1m                                     _\n");
	printf("                                    (_)\n");
	printf("[0m[33m[1m              |    .\n");
	printf("[0m[33m[1m          .   |L  /|   .         [0m[36m[1m _\n");
	printf("[0m[33m[1m      _ . |\\ _| \\--+._/| .       [0m[36m[1m(_)\n");
	printf("[0m[33m[1m     / ||\\| Y J  )   / |/| ./\n");
	printf("    J  |)'( |        ` F`.'/       [0m[36m[1m _\n");
	printf("[0m[33m[1m  -<|  F         __     .-<        [0m[36m[1m(_)\n");
	printf("[0m[33m[1m    | /       .-'[0m[36m[1m. [0m[33m[1m`.  /[0m[36m[1m-. [0m[33m[1mL___\n");
	printf("    J \\      <    [0m[36m[1m\\ [0m[33m[1m | | [38;5;8m[1mO[0m[36m[1m\\[0m[33m[1m|.-' [0m[36m[1m _\n");
	printf("[0m[33m[1m  _J \\  .-    \\[0m[36m[1m/ [38;5;8m[1mO [0m[36m[1m| [0m[33m[1m| \\  |[0m[33m[1mF    [0m[36m[1m(_)\n");
	printf("[0m[33m[1m '-F  -<_.     \\   .-'  `-' L__\n");
	printf("__J  _   _.     >-'  [0m[33m[1m)[0m[31m[1m._.   [0m[33m[1m|-'\n");
	printf("[0m[33m[1m `-|.'   /_.          [0m[31m[1m\\_|  [0m[33m[1m F\n");
	printf("  /.-   .                _.<\n");
	printf(" /'    /.'             .'  `\\\n");
	printf("  /L  /'   |/      _.-'-\\\n");
	printf(" /'J       ___.---'\\|\n");
	printf("   |\\  .--' V  | `. `\n");
	printf("   |/`. `-.     `._)\n");
	printf("      / .-.\\\n");
	printf("      \\ (  `\\\n");
	printf("       `.\\[0m\n");
	printf("I HATE THE DEMON KAHL!\n");
	printf("[?25h[?7h");

	return 0;
}